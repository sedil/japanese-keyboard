#ifndef KATAKANABOARD_H
#define KATAKANABOARD_H

#include "kanawindow.h"
#include <QWidget>
#include <QList>
#include <QPushButton>

class Kanawindow;

/**
 * @brief Creates a keyboardlayout with katakana
 */
class KatakanaBoard : public QWidget {
    Q_OBJECT

    Kanawindow *_kw;
    QList<QPushButton*> _katakanabuttonlist;

    void initComponents();
    void initLayout();
    void initSignalAndSlots();
public:
    explicit KatakanaBoard(Kanawindow *kw, QWidget *parent = nullptr);
    virtual ~KatakanaBoard();
signals:

};

#endif // KATAKANABOARD_H
