#include "crossfield.h"
#include <QGridLayout>
#include <QBoxLayout>

#include <QFileDialog>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

Kanawindow::Kanawindow(QWidget *parent)
    : QWidget(parent) {
    _hiraganamode = true;

    QString path = QFileDialog::getOpenFileName(this,tr("Open file"),".",tr("JSON (*.json)"));
    if(path.isEmpty()){
        return;
    }
    createKanamap(path);
    _hb = new HiraganaBoard(this);
    _kb = new KatakanaBoard(this);
    switchBoardlayout(0);
    initComponents();
    initLayout();
    initSignalAndSlots();
}

Kanawindow::~Kanawindow() { }

/**
 * @brief Kanawindow::initComponents creates all necessary gui elements
 */
void Kanawindow::initComponents(){
    _kanacontent = new QTextEdit();
    _kanaswitchlist = QList<QRadioButton*>();
    _kanaswitchlist.append(new QRadioButton("Hiragana"));
    _kanaswitchlist.append(new QRadioButton("Katakana"));
    _kanaswitchlist[0]->setChecked(true);
    _clearBtn = new QPushButton("Clear");
    _removeCharBtn = new QPushButton("Remove last");
}

/**
 * @brief Kanawindow::initLayout builds the gui with the components
 */
void Kanawindow::initLayout(){
    _layout = new QBoxLayout(QBoxLayout::TopToBottom);

    QGridLayout *kanaswitchlayout = new QGridLayout();
    kanaswitchlayout->addWidget(_kanaswitchlist[0], 0, 0);
    kanaswitchlayout->addWidget(_kanaswitchlist[1], 0, 1);
    kanaswitchlayout->addWidget(_removeCharBtn, 0, 2);
    kanaswitchlayout->addWidget(_clearBtn, 0, 3);

    QGridLayout *buttonlayout = new QGridLayout();
    buttonlayout->addLayout(kanaswitchlayout, 0, 2, 1, 10);
    buttonlayout->addWidget(_hb, 1, 0, 1, 12);
    buttonlayout->addWidget(_kb, 1, 0, 1, 12);
    buttonlayout->addWidget(new SpecialBoard(this), 1, 12, 1, 2);

    _layout->addWidget(_kanacontent);
    _layout->addLayout(buttonlayout);
    setMinimumSize(640, 480);
    setLayout(_layout);
    setWindowTitle("Japanese Keyboard");
    show();
}

/**
 * @brief Kanawindow::initSignalAndSlots connects all sender and receivers
 */
void Kanawindow::initSignalAndSlots(){
    for(unsigned short n = 0; n < _kanaswitchlist.size(); n++){
        connect(_kanaswitchlist[n], &QRadioButton::clicked, this, [this,n] {switchBoardlayout(n);});
    }
    connect(_clearBtn, &QPushButton::clicked, this, [this]{_kanacontent->clear();});
    connect(_removeCharBtn, &QPushButton::clicked, this, [this] {removeLastCharFromKanacontent();});
}

/**
 * @brief Kanawindow::createKanamap reads a jsonfile where all hiragana and katakana signs are
 * contained
 * @param path the path to the jsonfile
 */
void Kanawindow::createKanamap(const QString &path){
    QFile file;
    file.setFileName(path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString data = file.readAll();
    file.close();

    _hiraganamap = new QMap<QString, QList<QString>>();
    _katakanamap = new QMap<QString, QList<QString>>();
    _specialmap = new QMap<QString, QList<QString>>();

    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject obj = doc.object();
    QJsonArray hiraganas = obj.value("hiragana").toArray();
    QJsonArray katakanas = obj.value("katakana").toArray();
    QJsonArray specials = obj.value("special").toArray();

    for(auto n = 0; n < hiraganas.size(); n++){
        QJsonValue center_array = hiraganas[n];
        QString centerelement = center_array[0].toString();
        QJsonArray crosselementsarray = center_array[1].toArray();
        QList<QString> crosslist;
        for(auto element : crosselementsarray){
            QByteArray hex = QByteArray::fromHex(element.toString().toUtf8());
            crosslist.append(hex);
        }

        QByteArray hex = QByteArray::fromHex(centerelement.toUtf8());
        _hiraganamap->insert(hex, crosslist);
    }

    for(auto n = 0; n < katakanas.size(); n++){
        QJsonValue center_array = katakanas[n];
        QString centerelement = center_array[0].toString();
        QJsonArray crosselementsarray = center_array[1].toArray();
        QList<QString> crosslist;
        for(auto element : crosselementsarray){
            QByteArray hex = QByteArray::fromHex(element.toString().toUtf8());
            crosslist.append(hex);
        }

        QByteArray hex = QByteArray::fromHex(centerelement.toUtf8());
        _katakanamap->insert(hex, crosslist);
    }

    for(auto n = 0; n < specials.size(); n++){
        QJsonValue center_array = specials[n];
        QString centerelement = center_array[0].toString();
        QJsonArray crosselementsarray = center_array[1].toArray();
        QList<QString> crosslist;
        for(auto element : crosselementsarray){
            QByteArray hex = QByteArray::fromHex(element.toString().toUtf8());
            crosslist.append(hex);
        }

        QByteArray hex = QByteArray::fromHex(centerelement.toUtf8());
        _specialmap->insert(hex, crosslist);
    }
}

QMap<QString, QList<QString>>* Kanawindow::getHiraganamap() const {
    return _hiraganamap;
}

QMap<QString, QList<QString>>* Kanawindow::getKatakanamap() const {
    return _katakanamap;
}

QMap<QString, QList<QString>>* Kanawindow::getSpecialmap() const {
    return _specialmap;
}

/* SLOTS */

/**
 * @brief Kanawindow::switchBoardlayout switches between hiragana and katakana keyboardlayout
 * @param index determines which keyboardlayout should be loaded
 */
void Kanawindow::switchBoardlayout(unsigned short index) {
    if(index == 0){
        _hiraganamode = true;
        _hb->show();
        _kb->hide();
    } else if (index == 1){
        _hiraganamode = false;
        _hb->hide();
        _kb->show();
    }
}

/**
 * @brief Kanawindow::buildCross this slot is called to provide all crosselements for the centerelement
 * @param center the centerelement is a key for the crosselements
 * @param isSpecial if specialsigns should be loaded
 */
void Kanawindow::buildCross(QString center, bool isSpecial){
    QList<QString> values;
    if(_hiraganamode){
        values = _hiraganamap->value(center);
    } else {
        values = _katakanamap->value(center);
    }

    if(isSpecial){
        values = _specialmap->value(center);
    }

    CrossField *_kanainput = new CrossField(this, center);
    switch(values.size()){
    case 4:
        _kanainput->setWest(values.at(0));
        _kanainput->setNorth(values.at(1));
        _kanainput->setEast(values.at(2));
        _kanainput->setSouth(values.at(3));
        break;
    case 3:
        _kanainput->setWest(values.at(0));
        _kanainput->setNorth(values.at(1));
        _kanainput->setEast(values.at(2));
        break;
    case 2:
        _kanainput->setWest(values.at(0));
        _kanainput->setEast(values.at(1));
        break;
    case 1:
        _kanainput->setEast(values.at(0));
        break;
    }

    _kanainput->createCross();
}

/**
 * @brief Kanawindow::addKanaToTextblock adds a sign into the textfield
 * @param kana hiragana or katakana sign
 */
void Kanawindow::addKanaToTextblock(QString kana){
    _kanacontent->moveCursor(QTextCursor::End);
    _kanacontent->insertPlainText(kana);
    _kanacontent->moveCursor(QTextCursor::End);
}

void Kanawindow::removeLastCharFromKanacontent() {
    QString content = _kanacontent->toPlainText();
    content = content.remove(content.size() - 1,1);
   _kanacontent->clear();
   _kanacontent->setText(content);
}
