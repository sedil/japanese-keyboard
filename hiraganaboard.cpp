#include "hiraganaboard.h"
#include <QBoxLayout>
#include <QGridLayout>

HiraganaBoard::HiraganaBoard(Kanawindow *kw, QWidget *parent) : QWidget(parent) {
    _kw = kw;
    initComponents();
    initLayout();
    initSignalAndSlots();
}

HiraganaBoard::~HiraganaBoard() { }

/**
 * @brief HiraganaBoard::initComponents creates all necessary gui elements
 */
void HiraganaBoard::initComponents(){
    _hiraganabuttonlist = QList<QPushButton*>();
    const auto& keys = _kw->getHiraganamap()->keys();
    for(auto &elem : keys){
        QPushButton *btn = new QPushButton(elem);
        _hiraganabuttonlist.append(btn);
    }
}

/**
 * @brief HiraganaBoard::initLayout builds the gui with the components
 */
void HiraganaBoard::initLayout(){
    QGridLayout *layout = new QGridLayout();
    QGridLayout *kanalayout = new QGridLayout();
    kanalayout->addWidget(_hiraganabuttonlist[0], 0, 0, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[1], 0, 3, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[2], 0, 6, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[5], 0, 9, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[6], 1, 0, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[9], 1, 3, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[10], 1, 6, 1, 3);

    kanalayout->addWidget(_hiraganabuttonlist[13], 1, 9, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[15], 2, 0, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[16], 2, 3, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[17], 2, 6, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[21], 2, 9, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[23], 3, 0, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[24], 3, 3, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[26], 3, 6, 1, 3);
    kanalayout->addWidget(_hiraganabuttonlist[27], 3, 9, 1, 3);
    kanalayout->setSpacing(0);

    QGridLayout *smallkanalayout = new QGridLayout();
    smallkanalayout->addWidget(_hiraganabuttonlist[3], 0, 0, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[4], 0, 2, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[7], 0, 4, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[8], 0, 6, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[11], 0, 8, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[12], 0, 10, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[14], 1, 0, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[18], 1, 2, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[19], 1, 4, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[20], 1, 6, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[22], 1, 8, 1, 2);
    smallkanalayout->addWidget(_hiraganabuttonlist[25], 1, 10, 1, 2);
    smallkanalayout->setSpacing(0);

    layout->addLayout(kanalayout, 0, 0, 4, 12);
    layout->addLayout(smallkanalayout, 4, 0, 2, 12);
    layout->setSpacing(0);
    setLayout(layout);
}

/**
 * @brief HiraganaBoard::initSignalAndSlots connects all sender and receivers
 */
void HiraganaBoard::initSignalAndSlots(){
    for(unsigned int n = 0; n < _hiraganabuttonlist.size(); n++){
        connect(_hiraganabuttonlist[n], &QPushButton::clicked, _kw, [this,n]{_kw->buildCross(_hiraganabuttonlist[n]->text(),false);});
    }
}
