#include <QGridLayout>
#include <QDebug>
#include "crossfield.h"

/**
 * @brief CrossField::CrossField
 * @param kanawindow mainwindow instance
 * @param center the centerelement of the crossfield
 * @param parent
 */
CrossField::CrossField(Kanawindow *kanawindow, QString center, QWidget *parent) :
    QWidget(parent) {
    _kanawindow = kanawindow;
    _center = new QPushButton(center);
    _north = nullptr;
    _east = nullptr;
    _south = nullptr;
    _west = nullptr;
    connect(_center, &QPushButton::clicked, _kanawindow, [this]{
        _kanawindow->addKanaToTextblock(_center->text());
        close();});
    setWindowFlag(Qt::FramelessWindowHint);
    setWindowModality(Qt::ApplicationModal);
    setWindowTitle("Choose character");
}

CrossField::~CrossField(){
    delete _center;
    delete _north;
    delete _east;
    delete _south;
    delete _west;
}

/**
 * @brief CrossField::setNorth initializes the northbutton with content. This is optional.
 * Otherwise an empty button will be created
 * @param content the buttoncontent
 */
void CrossField::setNorth(QString content){
    _north = new QPushButton(content);
    connect(_north, &QPushButton::clicked, _kanawindow, [this]{
        _kanawindow->addKanaToTextblock(_north->text());
        close();});
}

/**
 * @brief CrossField::setEast initializes the eastbutton with content. This is optional.
 * Otherwise an empty button will be created
 * @param content the buttoncontent
 */
void CrossField::setEast(QString content){
    _east = new QPushButton(content);
    connect(_east, &QPushButton::clicked, _kanawindow, [this]{
        _kanawindow->addKanaToTextblock(_east->text());
        close();});
}

/**
 * @brief CrossField::setSouth initializes the southbutton with content. This is optional.
 * Otherwise an empty button will be created
 * @param content the buttoncontent
 */
void CrossField::setSouth(QString content){
    _south = new QPushButton(content);
    connect(_south, &QPushButton::clicked, _kanawindow, [this]{
        _kanawindow->addKanaToTextblock(_south->text());
        close();});
}

/**
 * @brief CrossField::setWest initializes the westbutton with content. This is optional.
 * Otherwise an empty button will be created
 * @param content the buttoncontent
 */
void CrossField::setWest(QString content){
    _west = new QPushButton(content);
    connect(_west, &QPushButton::clicked, _kanawindow, [this]{
        _kanawindow->addKanaToTextblock(_west->text());
        close();});
}

/* public slots */

/**
 * @brief CrossField::createCross Creates the final crossbuttons
 */
void CrossField::createCross(){
    QGridLayout *cross = new QGridLayout();
    cross->addWidget(_center, 1, 1);
    if(_north != nullptr){
        cross->addWidget(_north, 0, 1);
    } else {
        cross->addWidget(new QPushButton(), 0, 1);
    }
    if(_east != nullptr){
        cross->addWidget(_east, 1, 2);
    } else {
        cross->addWidget(new QPushButton(), 1, 2);
    }
    if(_south != nullptr){
        cross->addWidget(_south, 2, 1);
    } else {
        cross->addWidget(new QPushButton(), 2, 1);
    }
    if(_west != nullptr){
        cross->addWidget(_west, 1, 0);
    } else {
        cross->addWidget(new QPushButton(), 1, 0);
    }

    cross->setSpacing(0);
    setLayout(cross);
    QCursor cursor;
    setGeometry(cursor.pos().x() - 120,cursor.pos().y() - 60,0,0);
    show();
}
