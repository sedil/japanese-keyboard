#ifndef SPECIALBOARD_H
#define SPECIALBOARD_H

#include "kanawindow.h"
#include <QWidget>
#include <QList>
#include <QPushButton>

class Kanawindow;

/**
 * @brief Creates a keyboardlayout for specialsigns like dot, komma, quotationmarks
 */
class SpecialBoard : public QWidget {
    Q_OBJECT

    Kanawindow *_kw;
    QList<QPushButton*> _specialbuttons;
    void initComponents();
    void initLayout();
    void initSignalAndSlots();
public:
    explicit SpecialBoard(Kanawindow *kw, QWidget *parent = nullptr);
    virtual ~SpecialBoard();
signals:

};

#endif // SPECIALBOARD_H
