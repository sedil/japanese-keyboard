#include "katakanaboard.h"
#include <QBoxLayout>
#include <QGridLayout>

KatakanaBoard::KatakanaBoard(Kanawindow *kw, QWidget *parent) : QWidget(parent){
    _kw = kw;
    initComponents();
    initLayout();
    initSignalAndSlots();
}

KatakanaBoard::~KatakanaBoard() { }

/**
 * @brief KatakanaBoard::initComponents creates all necessary gui elements
 */
void KatakanaBoard::initComponents(){
    _katakanabuttonlist = QList<QPushButton*>();
    const auto &keys = _kw->getKatakanamap()->keys();
    for(auto &elem : keys){
        QPushButton *btn = new QPushButton(elem);
        _katakanabuttonlist.append(btn);
    }
}

/**
 * @brief KatakanaBoard::initLayout builds the gui with the components
 */
void KatakanaBoard::initLayout(){
    QGridLayout *layout = new QGridLayout();
    QGridLayout *kanalayout = new QGridLayout();
    kanalayout->addWidget(_katakanabuttonlist[0], 0, 0, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[1], 0, 3, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[2], 0, 6, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[5], 0, 9, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[6], 1, 0, 1 ,3);
    kanalayout->addWidget(_katakanabuttonlist[9], 1, 3, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[10], 1, 6, 1, 3);

    kanalayout->addWidget(_katakanabuttonlist[13], 1, 9, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[15], 2, 0, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[16], 2, 3, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[17], 2, 6, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[21], 2, 9, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[23], 3, 0, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[24], 3, 3, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[26], 3, 6, 1, 3);
    kanalayout->addWidget(_katakanabuttonlist[27], 3, 9, 1, 3);
    kanalayout->setSpacing(0);

    QGridLayout *smallkanalayout = new QGridLayout();
    smallkanalayout->addWidget(_katakanabuttonlist[3], 0, 0, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[4], 0, 2, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[7], 0, 4, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[8], 0, 6, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[11], 0, 8, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[12], 0, 10, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[14], 1, 0, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[18], 1, 2, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[19], 1, 4, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[20], 1, 6, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[22], 1, 8, 1, 2);
    smallkanalayout->addWidget(_katakanabuttonlist[25], 1, 10, 1, 2);
    smallkanalayout->setSpacing(0);

    layout->addLayout(kanalayout, 0, 0, 4, 12);
    layout->addLayout(smallkanalayout, 4, 0, 2, 12);
    layout->setSpacing(0);
    setLayout(layout);
}

/**
 * @brief KatakanaBoard::initSignalAndSlots connects all sender and receivers
 */
void KatakanaBoard::initSignalAndSlots(){
    for(unsigned short n = 0; n < _katakanabuttonlist.size(); n++){
        connect(_katakanabuttonlist[n], &QPushButton::clicked, _kw, [this,n] {_kw->buildCross(_katakanabuttonlist[n]->text(),false);});
    }
}
