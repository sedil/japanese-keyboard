#ifndef HIRAGANABOARD_H
#define HIRAGANABOARD_H

#include "kanawindow.h"
#include <QWidget>
#include <QPushButton>
#include <QList>
#include <QMap>

class Kanawindow;

/**
 * @brief Creates the hiragana keyboardlayout
 */
class HiraganaBoard : public QWidget {
    Q_OBJECT

    Kanawindow *_kw;
    QList<QPushButton*> _hiraganabuttonlist;

    void initComponents();
    void initLayout();
    void initSignalAndSlots();
public:
    explicit HiraganaBoard(Kanawindow *kw, QWidget *parent = nullptr);
    virtual ~HiraganaBoard();
signals:

};

#endif // HIRAGANABOARD_H
