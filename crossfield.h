#ifndef CROSSFIELD_H
#define CROSSFIELD_H

#include "kanawindow.h"
#include <QWidget>
#include <QPushButton>

/**
 * @brief This class provides buttons in a cross order
 * where you can choose between multiple signs
 */
class CrossField : public QWidget {
    Q_OBJECT

    Kanawindow* _kanawindow;
    QPushButton* _center;
    QPushButton* _north;
    QPushButton* _east;
    QPushButton* _south;
    QPushButton* _west;

public:
    explicit CrossField(Kanawindow *kanawindow, QString centercontent, QWidget *parent = nullptr);
    virtual ~CrossField();

    void setNorth(QString content);
    void setEast(QString content);
    void setSouth(QString content);
    void setWest(QString content);

    void createCross();

signals:

public slots:

};

#endif // CROSSFIELD_H
