#ifndef KANAWINDOW_H
#define KANAWINDOW_H

#include "hiraganaboard.h"
#include "katakanaboard.h"
#include "specialboard.h"
#include <QWidget>
#include <QBoxLayout>
#include <QList>
#include <QPushButton>
#include <QRadioButton>
#include <QTextEdit>

class HiraganaBoard;
class KatakanaBoard;
class SpecialBoard;

/**
 * @brief This is the mainwindow of this application. It creates a textfield and all buttons to
 * choose various hiragana or katakana
 */
class Kanawindow : public QWidget {
    Q_OBJECT

    HiraganaBoard *_hb;
    KatakanaBoard *_kb;
    QBoxLayout *_layout;
    QMap<QString, QList<QString>> *_hiraganamap;
    QMap<QString, QList<QString>> *_katakanamap;
    QMap<QString, QList<QString>> *_specialmap;
    QList<QRadioButton*> _kanaswitchlist;
    QPushButton *_clearBtn;
    QPushButton *_removeCharBtn;
    QTextEdit *_kanacontent;
    bool _hiraganamode;

    void initComponents();
    void initLayout();
    void initSignalAndSlots();
    void createKanamap(const QString &path);

public:
    Kanawindow(QWidget *parent = nullptr);
    virtual ~Kanawindow();

    QMap<QString, QList<QString>>* getHiraganamap() const;
    QMap<QString, QList<QString>>* getKatakanamap() const;
    QMap<QString, QList<QString>>* getSpecialmap() const;

public slots:
    void switchBoardlayout(unsigned short index);
    void buildCross(QString center, bool isSpecial);
    void addKanaToTextblock(QString kana);
    void removeLastCharFromKanacontent();
};

#endif // KANAWINDOW_H
