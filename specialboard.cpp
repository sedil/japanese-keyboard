#include "specialboard.h"

SpecialBoard::SpecialBoard(Kanawindow *kw, QWidget *parent) : QWidget(parent) {
    _kw = kw;
    initComponents();
    initLayout();
    initSignalAndSlots();
}

SpecialBoard::~SpecialBoard(){ }

/**
 * @brief SpecialBoard::initComponents creates all necessary gui elements
 */
void SpecialBoard::initComponents(){
    _specialbuttons = QList<QPushButton*>();
    const auto &keys = _kw->getSpecialmap()->keys();
    for(auto &elem : keys){
        QPushButton *btn = new QPushButton(elem);
        _specialbuttons.append(btn);
    }
}

/**
 * @brief SpecialBoard::initLayout builds the gui with the components
 */
void SpecialBoard::initLayout(){
    QGridLayout *layout = new QGridLayout();
    layout->addWidget(_specialbuttons[0], 0, 0, 1, 2);
    layout->addWidget(_specialbuttons[1], 1, 0, 1, 2);
    layout->addWidget(_specialbuttons[2], 2, 0, 1, 2);
    layout->addWidget(_specialbuttons[3], 3, 0, 1, 2);
    layout->addWidget(_specialbuttons[4], 4, 0, 1, 2);
    layout->setSpacing(0);
    setLayout(layout);
}

/**
 * @brief SpecialBoard::initSignalAndSlots connects all sender and receivers
 */
void SpecialBoard::initSignalAndSlots(){
    for(unsigned short n = 0; n < _specialbuttons.size(); n++){
        connect(_specialbuttons[n], &QPushButton::clicked, this, [this,n] {_kw->buildCross(_specialbuttons[n]->text(),true);});
    }
}
